<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\User;
use App\Models\UserAccount;

class AdminController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function userAccountList() {
        return response()->json(User::with('user_account', 'user_info')->get());
    }

    public function userInformationList() {
        return response()->json(Information::with('user.user_info')->get());
    }

    public function approveAccount($id) {
        $user_account = UserAccount::where('id', $id)->first();
        $user_account->update(['status' => 'APPROVED']);
        return response()->json(['message' => 'User has been approved.']);
    }

    public function declineAccount($id) {
        $user_account = UserAccount::where('id', $id)->first();
        $user_account->update(['status' => 'DECLINED']);
        return response()->json(['message' => 'User has been declined.']);
    }

    public function unblockInformation($id) {
        $information = Information::where('id', $id)->first();
        $information->update(['blocked' => false]);
        return response()->json(['message' => 'Information has been unblocked.']);
    }

    public function blockInformation($id) {
        $information = Information::where('id', $id)->first();
        $information->update(['blocked' => true]);
        return response()->json(['message' => 'Information has been blocked.']);
    }

    public function getTotalUser() {
        return response()->json(['total' => User::count()]);
    }

    public function getTotalInformation() {
        return response()->json(['total' => Information::count()]);

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\InformationTag;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InformationController extends Controller
{
    //
    public function index() {
        return response()->json(User::where('id', Auth::id())->with('informations')->get());
    }

    public function tagList() {
        return response()->json(Tag::all());
    }

    public function store(Request $request) {


        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'featured_image' => 'required',
        ]);

        $data = [
            'slug' => $request->title,
            'title' => $request->title,
            'content' => $request->content,
            'featured_image' => $request->featured_image,
            'blocked' => false,
            'user_id' => Auth::id()
        ];

        $information = Information::create($data);

        foreach($request->tags as $tag) {
            InformationTag::create(['information_id' => $information->id, 'tag_id' => $tag]);
        }

        return response()->json(['message' => "Information saved"]);
    }

    public function update(Request $request, $id) {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'featured_image' => 'required',
        ]);

        $data = [
            'title' => $request->title,
            'content' => $request->content,
            'featured_image' => $request->featured_image,
            'slug' => Information::updateUniqueSlug($request->title),
            'user_id' => Auth::id()
        ];

        Information::where('id', $id)->update($data);

        InformationTag::where('information_id', $id)->delete();
        foreach($request->tags as $tag) {
            InformationTag::create(['information_id' => $id, 'tag_id' => $tag]);
        }

        return response()->json(['message' => "Information saved"]);
    }

    public function destroy($id) {
        InformationTag::where('information_id', $id)->delete();
        $information = Information::where('id', $id)->first();
        $this->deleteFileFromServer($information->featured_image);
        Information::destroy($id);

    
    }

    public function deleteFileFromServer($filename){
        $filePath = public_path().'/uploads/'.$filename;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFeaturedImage(Request $request){
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $picName);
        return $picName;
    }

    public function informationCount() {
        return response()->json(User::where('id', Auth::id())->withCount('informations')->get());
    }
}

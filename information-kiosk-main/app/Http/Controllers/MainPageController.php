<?php

namespace App\Http\Controllers;

use App\Models\Information;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    //
    public function index() {
        return response()->json(Information::where('blocked', 0)->with('user.user_info')->get());
    }

    public function singleInfo($slug) {
       
        $info = Information::with(['user.user_info'])->where('slug', $slug)->first();
        return response()->json($info, 200);
    }
}

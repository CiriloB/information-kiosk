<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{
    //

    
     /**
     * Create a new AdminAuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user', ['except' => ['login', 'register']]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
        ]);

        $user_account = [
            'username' => $request->username,
            'password' =>   Hash::make($request->password),
            'status' => 'PENDING'
        ];

        $user_account = UserAccount::create($user_account);

        $user_info = [
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
        ];

        $user_info = UserInfo::create($user_info);

        $user = [
            'user_account_id' => $user_account->id,
            'user_info_id' => $user_info->id,
        ];

        User::create($user);


        return response()->json([ "message" => "Registered succesfully."]);
    }

    public function login(Request $request)
    {

        if (! $token = auth()->guard('user')->attempt(['username' => $request->username, 'password' => $request->password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(['user' => User::with('user_info')->find(Auth::id())]);
    }

    public function logout()
    {

        auth('user')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('user')->factory()->getTTL() * 60,
            'user' => User::with('user_info')->find(Auth::id())

        ]);
    }
}

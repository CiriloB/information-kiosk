<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InformationTag extends Model
{
    use HasFactory;

    protected $table = "information_tags";

    protected $guarded = [];
    
    public function tags() {
        return $this->hasMany(InformationTag::class, 'information_id', 'id');
    }
}

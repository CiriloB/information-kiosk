<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function informations() {
        return $this->hasMany(Information::class, 'user_id', 'id');
    }

    public function user_account() {
        return $this->hasOne(UserAccount::class,  'id');
    }

    public function user_info() {
        return $this->hasOne(UserInfo::class, 'id');
    }
}

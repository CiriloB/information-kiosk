<?php

namespace Database\Seeders;

use App\Models\Information;
use Carbon\Carbon;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class InformationSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $informations = [
            [
                'title' => 'Information Title 1 Example',
                'content' => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, maxime consequuntur doloribus hic explicabo temporibus, quis animi quia totam expedita eveniet necessitatibus incidunt. Aspernatur quaerat dolores quo quos fugiat aperiam!",
                'blocked' => false,
                'featured_image' => 'info_image1.png',
                'slug' =>  'information-title-1-example',
                'user_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title' => 'Information Title 2 Example',
                'content' => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, maxime consequuntur doloribus hic explicabo temporibus, quis animi quia totam expedita eveniet necessitatibus incidunt. Aspernatur quaerat dolores quo quos fugiat aperiam!",
                'blocked' => false,
                'featured_image' => 'info_image2.png',
                'slug' =>  'information-title-2-example',
                'user_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title' => 'Information Title 3 Example',
                'content' => "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, maxime consequuntur doloribus hic explicabo temporibus, quis animi quia totam expedita eveniet necessitatibus incidunt. Aspernatur quaerat dolores quo quos fugiat aperiam!",
                'blocked' => false,
                'featured_image' => 'info_image3.png',
                'slug' =>  'information-title-3-example',
                'user_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ];

        Information::insert($informations);
    }
}

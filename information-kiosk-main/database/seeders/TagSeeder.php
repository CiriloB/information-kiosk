<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $tags = [
            ['name' => 'Anti-Information'],
            ['name' => 'Data'],
            ['name' => 'Fact'],
            ['name' => 'Asset'],
            ['name' => 'Economics'],
            ['name' => 'Good'],
            ['name' => 'Need'],
            ['name' => 'Knowledge'],
            ['name' => 'Misinformation'],
            ['name' => 'Storytelling'],
            ['name' => 'Specifications'],
            ['name' => 'Sensory'],
            ['name' => 'Visual'],
            ['name' => 'Visual'],
            ['name' => 'Industry'],
            ['name' => 'Announcement'],
            ['name' => 'Scholarship'],
        ];

        foreach($tags as $tag) {
            Tag::create($tag);
        }
    }
}

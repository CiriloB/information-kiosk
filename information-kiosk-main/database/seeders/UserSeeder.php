<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserInfo;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $user_account = [
            ['username' => 'cirilob', 'password' => Hash::make('cirilob'), 'status' => 'PENDING', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),],
            ['username' => 'wallinb', 'password' => Hash::make('wallinb'), 'status' => 'PENDING', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),],
            ['username' => 'kathleeni', 'password' => Hash::make('kathleeni'), 'status' => 'PENDING', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),],
            ['username' => 'derickd', 'password' => Hash::make('derickd'), 'status' => 'PENDING', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),]
        ];
        UserAccount::insert($user_account);

        $user_infos = [
            ['first_name' => 'Cirilo', 'middle_name' => 'Espinisin', 'last_name' => 'Bucatcat', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_name' => 'Wallin', 'middle_name' => 'Alfredo', 'last_name' => 'Badidles', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_name' => 'Kathleen', 'middle_name' => 'Alapoop', 'last_name' => 'Igarta', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['first_name' => 'Derick Justin', 'middle_name' => 'Moral', 'last_name' => 'Durante', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now(),],

        ];

        UserInfo::insert($user_infos);

        $users = [
            ['user_account_id' => 1, 'user_info_id' => 1,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['user_account_id' => 2, 'user_info_id' => 2,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['user_account_id' => 3, 'user_info_id' => 3,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['user_account_id' => 4, 'user_info_id' => 4,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        User::insert($users);


    }
}

require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toast from "vue-toastification";
import VueFileAgent from 'vue-file-agent';
import VueFileAgentStyles from 'vue-file-agent/dist/vue-file-agent.css';

import "vue-toastification/dist/index.css";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Toast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 4,
    hideProgressBar: true,
});

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueFileAgent);


const app = new Vue({
    el: '#app',
    router,
    store,
    components: { App }
});

import Vue from 'vue';
import VueRouter from 'vue-router';
import NotFound from '../pages/NotFound.vue'
// Admin
import Admin from '../pages/Admin/Admin.vue'
import AdminLogin from '../pages/Admin/Login.vue'
import AdminHome from '../pages/Admin/Home.vue'
import AdminDashboard from '../pages/Admin/Dashboard.vue'
import AdminAccount from '../pages/Admin/Account.vue'
import AdminInformation from '../pages/Admin/Information.vue'
// User

import User from '../pages/User/User.vue'
import UserLogin from '../pages/User/Login.vue'
import UserRegister from '../pages/User/Register.vue'
import UserHome from '../pages/User/Home.vue'
import UserDashboard from '../pages/User/Dashboard.vue'
import UserInformation from '../pages/User/Information.vue'

import Home from '../pages/Home.vue'
import Info from '../pages/Info.vue'

const routes = [
    {
        path: '*',
        name: 'NotFound',
        component: NotFound
    },
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/:slug",
        name: "Info",
        component: Info,
    },

    {
        path: '/admin',
        component: Admin,
        children: [
            { path: 'login', name: 'Admin Login', component: AdminLogin },
            {
                path: '',
                component: AdminHome,
                children: [
                    { path: 'dashboard', name: 'Admin Dashboard', component: AdminDashboard },
                    { path: 'account', name: 'Admin Account', component: AdminAccount },
                    { path: 'information', name: 'Admin Information', component: AdminInformation },
                    { path: "", redirect: "dashboard" }
                ]
            },
            { path: "", redirect: "" }
        ]
    },
    {
        path: '/user',
        component: User,
        children: [
            { path: 'login', name: 'User Login', component: UserLogin },
            { path: 'register', name: 'User Register', component: UserRegister },

            {
                path: '',
                component: UserHome,
                children: [
                    { path: 'dashboard', name: 'User Dashboard', component: UserDashboard },
                    { path: 'information', name: 'User Information', component: UserInformation },
                    { path: "", redirect: "dashboard" }
                ]
            },
            { path: "", redirect: "" }
        ]
    },
];


Vue.use(VueRouter);

// router.beforeEach((to, from, next) => {
//     if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('access_token')) {
//         next({ path: '/login' })
//     }
//     else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('access_token') && localStorage.getItem('isAdmin')) {
//         next({ path: '/admin/' });
//     }
//     else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('access_token') && localStorage.getItem('isUser')) {
//         next({ path: '/student/' });
//     }
//     else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('access_token') && localStorage.getItem('isUser')) {
//         next({ path: '/student/' });
//     }
//     else {
//         next();
//     }
// });

export default new VueRouter({
    history: true,
    mode: "history",
    routes
});


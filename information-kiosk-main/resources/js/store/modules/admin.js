import axios from "../../axios";

const AUTH = "auth/admin"
export default {
    namespaced: true,
    state: {
        token: localStorage.getItem('access_token') || '',
        user: {},
        information_count: 0,
        informations: {},
        user_count: 0,
        user_accounts: {}
    },
    getters: {
        GET_TOKEN: (state) => state.token,
        GET_USER: (state) => state.user,
        GET_TOTAL_INFORMATION: (state) => state.information_count,
        GET_TOTAL_USER: (state) => state.user_count,
        GET_USER_ACCOUNTS: (state) => state.user_accounts,
        GET_INFORMATIONS: (state) => state.informations,

    },
    mutations: {
        SET_USER: (state, user) => {
            state.user = user;
            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            localStorage.setItem('isAdmin', 'true');
            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_USER: (state) => {
            localStorage.removeItem("access_token");
            state.token = "";
            axios.defaults.headers.common["Authorization"] = "";
        },

        SET_TOTAL_INFORMATION: (state, total) => state.information_count = total,
        SET_TOTAL_USER: (state, total) => state.user_count = total,
        SET_USER_ACCOUNTS: (state, accounts) => state.user_accounts = accounts,
        SET_INFORMATIONS: (state, informations) => state.informations = informations,


    },
    actions: {
        LOGOUT: async ({ commit }) => {
            const res = await axios.post(`${AUTH}/logout?`)
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        LOGIN: async ({ commit }, user) => {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_TOKEN", response.data.access_token);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        CHECK: async ({ commit }) => {
            const res = await axios.post(
                `${AUTH}/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        USER_COUNT: async ({ commit }) => {
            const res = await axios.get('getTotalUser')
                .then((response) => {
                    commit("SET_TOTAL_USER", response.data.total);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        INFORMATION_COUNT: async ({ commit }) => {
            const res = await axios.get('getTotalInformation')
                .then((response) => {
                    commit("SET_TOTAL_INFORMATION", response.data.total);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        USER_ACCOUNTS: async ({ commit }) => {
            await axios.get('user_account_list')
                .then((response) => {
                    commit("SET_USER_ACCOUNTS", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },

        APPROVE_ACCOUNT: async ({ commit }, id) => {
            const res = await axios.post(`approve_account/${id}`)
                .then((response) => {

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
        DECLINE_ACCOUNT: async ({ commit }, id) => {
            const res = await axios.post(`decline_account/${id}`)
                .then((response) => {

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },

        INFORMATIONS: async ({ commit }) => {
            await axios.get('user_information_list')
                .then((response) => {
                    commit("SET_INFORMATIONS", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },

        UNBLOCK_INFORMATION: async ({ commit }, id) => {
            const res = await axios.post(`unblock_information/${id}`)
                .then((response) => {

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },

        BLOCK_INFORMATION: async ({ commit }, id) => {
            const res = await axios.post(`block_information/${id}`)
                .then((response) => {

                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
            return res;
        },
    }
}
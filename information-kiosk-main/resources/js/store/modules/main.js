import axios from "../../axios";
export default {
    namespaced: true,
    state: {
        information: {},
        informations: {}
    },
    mutations: {
        SET_INFORMATION(state, data) {
            state.information = data;
        },
        SET_INFORMATIONS(state, data) {
            state.informations = data;
        },
    },
    getters: {
        GET_INFORMATION(state) {
            return state.information;
        },
        GET_INFORMATIONS(state) {
            return state.informations.filter((information) => {
                return information.blocked.toString() == '0'
            });
        },
    },
    actions: {
        async INFORMATION({ commit }, slug) {
            const res = await axios.get(`main_single_info/${slug}`)
                .then((response) => {
                    commit("SET_INFORMATION", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res
        },

        async INFORMATIONS({ commit }) {
            await axios.get("main_information_list")
                .then((response) => {
                    commit("SET_INFORMATIONS", response.data);
                })
                .catch((error) => {
                    return error.response;
                });
        },
    }
}
import axios from "../../axios";

const AUTH = "auth/user"
export default {
    namespaced: true,
    state: {
        token: localStorage.getItem('access_token') || '',
        information_count: 0,
        informations: {},
        user: {},
        tags: {}
    },
    getters: {
        GET_TOKEN: (state) => state.token,
        GET_USER: (state) => state.user,
        GET_TOTAL_INFORMATION: (state) => state.information_count,
        GET_INFORMATIONS: (state) => state.informations,
        GET_TAGS: (state) => state.tags,


    },
    mutations: {
        SET_USER: (state, user) => {
            state.user = user;
            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        SET_TOKEN: (state, token) => {
            localStorage.setItem("access_token", token);
            localStorage.setItem('isAdmin', 'true');
            state.token = token;

            const BEARER_TOKEN = localStorage.getItem("access_token") || "";
            axios.defaults.headers.common["Authorization"] = `Bearer ${BEARER_TOKEN}`;
        },
        UNSET_USER: (state) => {
            localStorage.removeItem("access_token");
            state.token = "";
            state.user = "";
            axios.defaults.headers.common["Authorization"] = "";
        },
        SET_TOTAL_INFORMATION: (state, total) => state.information_count = total,
        SET_INFORMATIONS: (state, informations) => state.informations = informations,
        SET_TAGS: (state, tags) => state.tags = tags,


    },
    actions: {
        REGISTER: async ({ commit }, data) => {
            const res = await axios.post(`${AUTH}/register`, data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        LOGOUT: async ({ commit }) => {
            const res = await axios.post(`${AUTH}/logout?`)
                .then((response) => {
                    commit("UNSET_USER");
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },
        LOGIN: async ({ commit }, user) => {
            const res = await axios.post(`${AUTH}/login`, user)
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    commit("SET_TOKEN", response.data.access_token);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        CHECK: async ({ commit }) => {
            const res = await axios.post(
                `${AUTH}/me?token=` + localStorage.getItem("access_token")
            )
                .then((response) => {
                    commit("SET_USER", response.data.user);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        INFORMATION_COUNT: async ({ commit }) => {
            const res = await axios.get('informationCount')
                .then((response) => {
                    commit("SET_TOTAL_INFORMATION", response.data[0].informations_count);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return res;
        },

        INFORMATIONS: async ({ commit }) => {
            await axios.get('informations')
                .then((response) => {
                    commit("SET_INFORMATIONS", response.data[0].informations);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },

        TAGS: async ({ commit }) => {
            await axios.get('tag_list')
                .then((response) => {
                    commit("SET_TAGS", response.data);
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });
        },

        STORE: async ({ commit }, data) => {
            const response = await axios.post('informations', data)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        DESTROY: async ({ commit }, id) => {
            const response = await axios.delete(`informations/${id}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        UPDATE: async ({ commit }, { id, information }) => {
            const response = await axios.put(`informations/${id}`, information)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },

        DELETE_IMAGE: async ({ commit }, fileName) => {
            const response = await axios.post(`deleteFileFromServer/${fileName}`)
                .then((response) => {
                    return response;
                })
                .catch((error) => {
                    return error.response;
                });

            return response
        },
    }
}
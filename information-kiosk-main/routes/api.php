<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\InformationContoller;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\UserAuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {

    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminAuthController::class, 'login']);
        Route::post('logout', [AdminAuthController::class, 'logout']);
        Route::post('me', [AdminAuthController::class, 'me']);
    });
    
    Route::group(['prefix' => 'user'], function (){
        Route::post('login', [UserAuthController::class, 'login']);
        Route::post('register', [UserAuthController::class, 'register']);
        Route::post('logout', [UserAuthController::class, 'logout']);
        Route::post('me', [UserAuthController::class, 'me']);
    });
});

Route::get('user_account_list', [AdminController::class, 'userAccountList']);
Route::get('user_information_list', [AdminController::class, 'userInformationList']);
Route::post('approve_account/{id}', [AdminController::class, 'approveAccount']);
Route::post('decline_account/{id}', [AdminController::class, 'declineAccount']);
Route::post('unblock_information/{id}', [AdminController::class, 'unblockInformation']);
Route::post('block_information/{id}', [AdminController::class, 'blockInformation']);
Route::get('getTotalUser', [AdminController::class, 'getTotalUser']);
Route::get('getTotalInformation', [AdminController::class, 'getTotalInformation']);

Route::apiResource('informations', InformationController::class);
Route::get('informationCount', [InformationController::class, 'informationCount']);
Route::post('uploadFeaturedImage', [InformationController::class, 'uploadFeaturedImage']);
Route::post('deleteFileFromServer/{filename}', [InformationController::class, 'deleteFileFromServer']);
Route::get('tag_list', [InformationController::class, 'tagList']);

Route::get('main_information_list', [MainPageController::class, 'index']);
Route::get('main_single_info/{slug}', [MainPageController::class, 'singleInfo']);





<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LaravelSetUpTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_laravel_project_setup_is_successfull()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}

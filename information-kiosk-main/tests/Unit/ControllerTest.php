<?php

namespace Tests\Unit;

use App\Models\Admin;
use App\Models\Information;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserInfo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ControllerTest extends TestCase
{
    use RefreshDatabase;

    // CRUD Anay

    public function login_admin_account() {

        $admin = Admin::create(['name' => 'Admin1', 'username' => 'admin1', 'password' => Hash::make("admin1")]);
        $account = [
            'username' => $admin->username,
            'password' => 'admin1',
        ];

        $response = $this->call('POST', 'api/auth/admin/login?', $account);
        return $response;

    }

    public function login_user_account()
    {
        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'PENDING']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);

        $account = [
            'username' => $user_account->username,
            'password' => 'johndoe',
        ];

        $response = $this->call('POST', 'api/auth/user/login?', $account);

        return $response;
    }

    public function test_admin_can_approve_user_account() {

        $response = $this->login_admin_account();

        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'PENDING']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);
        $this->call('POST', 'api/approve_account/'. $user->id . '/?token='. $response['access_token'])->assertStatus(200);
    }

    // Info CRUD
    public function test_user_can_store_information() {

        $response = $this->login_user_account();

        $data = [
            'title' => 'Title 1',
            'content' => 'this is a content',
            'featured_image' => 'featured_image.png',
            'slug' => 'this-is-slug',
        ];
        
        $this->call('POST', 'api/informations?token='. $response['access_token'], $data)->assertStatus(200);

    }

    public function test_user_can_update_information() {
        
        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'PENDING']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);

        $account = [
            'username' => $user_account->username,
            'password' => 'johndoe',
        ];

        $response = $this->call('POST', 'api/auth/user/login?', $account);

        $data = [
            'title' => 'Title 1',
            'content' => 'this is a content',
            'featured_image' => 'featured_image.png',
            'slug' => 'this-is-slug',
            'user_id' => $user->id
        ];

        $information = Information::create($data);

        $data = [
            'title' => 'Title',
            'content' => 'this is a content',
            'featured_image' => 'featured_image.png',
            'slug' => 'this-is-slug',
            'user_id' => $user->id
        ];
        
        $this->call('PUT', 'api/informations/'. $information->id .'/?token='. $response['access_token'], $data)->assertStatus(200);
    }

    public function test_user_can_destroy_information() {

        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'PENDING']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);

        $account = [
            'username' => $user_account->username,
            'password' => 'johndoe',
        ];

        $response = $this->call('POST', 'api/auth/user/login?', $account);

        $data = [
            'title' => 'Title 1',
            'content' => 'this is a content',
            'featured_image' => 'featured_image.png',
            'slug' => 'this-is-slug',
            'user_id' => $user->id
        ];

        $information = Information::create($data);
        
        $this->call('DELETE', 'api/informations/'. $information->id .'/?token='. $response['access_token'])->assertStatus(200);

    }

    public function test_user_can_get_all_informations() {
        

        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'PENDING']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);

        $account = [
            'username' => $user_account->username,
            'password' => 'johndoe',
        ];

        $response = $this->call('POST', 'api/auth/user/login?', $account);

        $data = [
            'title' => 'Title 1',
            'content' => 'this is a content',
            'featured_image' => 'featured_image.png',
            'slug' => 'this-is-slug',
            'user_id' => $user->id
        ];

        Information::create($data);

        $this->call('GET', 'api/informations?token='. $response['access_token'])->assertStatus(200);

    }

}

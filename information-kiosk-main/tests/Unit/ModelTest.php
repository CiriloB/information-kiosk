<?php

namespace Tests\Unit;

use App\Models\Information;
use App\Models\InformationTag;
use App\Models\Tag;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserInfo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase;

    public function test_informations_belongs_to_user() {
       
        // Create a user
        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'APPROVED']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);
     
        $information = Information::create(['title' => 'Sample Title 1', 'content' => 'this is a content', 'featured_image' => 'featured image', 'slug' => 'sample-slug', 'user_id' => $user->id]);

       $this->assertInstanceOf(User::class, $information->user);
       $this->assertEquals(1, $information->user->count());

    }

    public function test_user_has_many_informations() {
       
        // Create a user
        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'APPROVED']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);
        Information::create(['title' => 'Sample Title 1', 'content' => 'this is a content', 'featured_image' => 'featured image', 'slug' => 'sample-slug', 'user_id' => $user->id], ['title' => 'Sample Title 1', 'content' => 'this is a content', 'featured_image' => 'featured image', 'slug' => 'sample-slug', 'user_id' => $user->id]);
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $user->informations);
    }

    public function test_information_has_many_tags() {
        $user_account = UserAccount::create(['username' => 'johndoe', 'password' => Hash::make("johndoe"), 'status' => 'APPROVED']);
        $user_info = UserInfo::create(['first_name' => 'John', 'middle_name' => 'Kape', 'last_name' => 'Doe']);
        $user = User::create(['user_info_id' => $user_info->id, 'user_account_id' => $user_account->id]);
        $information = Information::create(['title' => 'Sample Title 1', 'content' => 'this is a content', 'featured_image' => 'featured image', 'slug' => 'sample-slug', 'user_id' => $user->id], ['title' => 'Sample Title 1', 'content' => 'this is a content', 'featured_image' => 'featured image', 'slug' => 'sample-slug', 'user_id' => $user->id]);
        
        $tag = Tag::create([
            'name' => 'Kape'
        ],
        [
            'name' => 'Kape1'
        ]);

        $information_tags = InformationTag::create(
            ['information_id' => $information->id, 'tag_id' => 1],
            ['information_id' => $information->id, 'tag_id' => 2],
        );

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $information_tags->tags);

    }
}

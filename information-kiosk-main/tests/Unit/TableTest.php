<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class TableTest extends TestCase
{
    public function test_a_admins_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('admins', [
              'id', 'username', 'password'
          ]), 1);
    }

    public function test_a_users_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('users', [
              'id', 'user_info_id', 'user_account_id'
          ]), 1);
    }

    public function test_a_user_infos_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('user_infos', [
              'id', 'first_name', 'middle_name', 'last_name', 
          ]), 1);
    }

    public function test_a_user_accounts_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('user_accounts', [
              'id', 'username', 'password', 'status'
          ]), 1);
    }

    public function test_a_informations_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('informations', [
              'id', 'title', 'content', 'featured_image', 'slug', 'user_id'
          ]), 1);
    }

    public function test_a_tags_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('tags', [
              'id', 'name'
          ]), 1);
    }

    public function test_a_information_tags_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('information_tags', [
              'id', 'information_id', 'tag_id'
          ]), 1);
    }
}
